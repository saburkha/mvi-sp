## Report

### ___Embedding-based (personalized) search___

I divided my work into three tasks:
1. Building a search engine
2. Learning to rank based on tf-idf vectors
3. An example of using a recommendation system based on an indexed and ranked list of documents


## **Building a search engine:**
There is a database of documents. A search query comes in. 
We need to return *k* most matching documents for this query (not necessarily sorted).

_Description of the searcher:_

>We have a vector (conditional) representation of all queries. The query is vectorized, then we try to find the nearest vector among the documents (answers) through an index. 
The index is a mapping from the document number to the document, which is on disk.


![How Search works](/searchblock.png)

I will use word frequency search.
First we create a database (collection) of documents, the index will be built on it.
The database must contain the _ID_ and _content_ of the document.
Then we build a lot of json documents, save them to a folder. Then it will be indexed with pyserini (which builds tf-idf vectors).
Then we need to calculate score for all documents and queries.

___The BM25 weighting model___

Let a query Q containing words 𝑞1,...,𝑞𝑛 is given, 
then function BM25 gives the following evaluation of the relevance of document 𝐷 to query 𝑄: 
>$`\text{score}(D,Q) = \sum_{i=1}^{n} \text{IDF}(q_i) \cdot \frac{tf(q_i, D) \cdot (k_1 + 1)}{tf(q_i, D) + k_1 \cdot (1 - b + b \cdot \frac{|D|}{\text{avgdl}})},`$ 

where 𝑡𝑓(𝑞𝑖,𝐷) is frequency of word 𝑞𝑖 in document 𝐷, |𝐷| is length of document (number of words in it), and 𝑣𝑔𝑑𝑙 is average length of document in collection. 𝑘1 and 𝑏 are free coefficients.


BM-25 - counts how often the i-th token occurs in all documents. Then it counts the frequency of the word in a particular document and rarely in other documents. k1 - coefficient, how important, how long the document is, etc. (if we set K1=0, we will get IDF vector)

___Dirichlet language modelling___ (QLD)

Idea: Dirichlet latent placement (reference to thematic models)

___RM3___

Let a query 𝑄 containing the words 𝑞1,...,𝑞𝑛 be given, then the BM25 function gives the following estimate of the relevance of token 𝑡 to query 𝑄:
>$`score\bigr(t, Q\bigr) = \alpha \cdot \text{softmax}\left(\sum_{d\in PRD}\left[\frac{tf\bigr(t, d\bigr)}{|d|}\cdot\prod_{i=1}^{k}\frac{tf\bigr(q_i, d\bigr)+\mu p\bigr(q_i\bigr)}{|d|+\mu}\right]\right) + \left(1-\alpha\right)\cdot\left(\frac{tf\bigr(t, Q\bigr)}{|Q|}\right),`$

where 𝑡𝑓(𝑡,𝑑) is the frequency of the word 𝑡 in the document 𝑑, |𝑑| is the length of the document (number of words in it), and 𝛼 and 𝜇 are the free factors.

Unlike BM25, this has a different Score function.

## **Learning to rank based on tf-idf vectors:**

We have some set of objects which is partially ordered. For some pairs of objects there is a given order that _i_ object is better than _j_ object.

We need to build a model which will put some other object in this sequence of objects.


![Ranking model](/learningrank.png)

_Model Description:_

> There is a set of documents in the database.
> A search query (user query) comes in.
> This query needs to be ranked somehow. Understand which queries are better or worse
> They are ranked according to the index (yes), but we need to find the top-k according to the ranking model

It has to be trained somehow. To do this, we need a partitioned database, so that a training sample is created for each query from the k-found documents. For it we train the model, and then, when new queries will come, we will be able to rank new documents.

_Explanation of the metric:_

>$`DCG@K = \sum_{k=1}^{K}\frac{r_{true}\bigr(\pi^{-1}\bigr(k\bigr)\bigr)}{\log_2\bigr(k+1\bigr)}, \quad IDCG@K=\sum_{k=1}^{K}\frac{1}{\log_2\bigr(k+1\bigr)}`$

>$`nDCG@K = \frac{DCG@K}{IDCG@K}`$

This metric tells (r true) whether the object that got to position K after ranking (K is the number of queries that should be).
> nDCG metric is normalized DCG metric

> DCG metric gives a value larger than 1, with normalization (right) is normalized

The test produces a Score of some metric for each query.
Within each query, it performs a ranking using the "NDCG@K" metric and outputs how good that metric is (the higher the value, the better).

## __Recommendation system:__

There are sets user(query), item(answer), rating(rank of answer to query).

The problem here is that we want to recommend some items to this user. Previously, the user has already rated some items. We need to give the user an item, which he is likely to give a rating of 5.

**-----------------------------------------------------------------------**
